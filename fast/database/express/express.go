package express

import (
	"fmt"
	"gitee.com/ccait-dev/fast-api/fast/config"
	"gitee.com/ccait-dev/fast-api/fast/utils/jsonUtil"
	"github.com/emirpasic/gods/sets/hashset"
	"github.com/emirpasic/gods/trees"
	"strings"
)

const FIELD_REGEX = "[\\w\\p{Han}][\\w\\d\\p{Han}]*"

type ExprInfo struct {
	Value          string
	Datasource     string
	Table          string
	Column         string
	Expr           string
	Function       string
	ConditionInfos []ConditionInfo
	FieldList      []string
	GroupList      hashset.Set
	OrderByList    hashset.Set
	tree           trees.Tree
}

func GetIn(field string, val string, m []string) (string, bool) {
	arr := strings.Split(m[1], ",")
	result := make([]string, 0)
	for _, v := range arr {
		if len(strings.TrimSpace(v)) == 0 {
			continue
		}
		result = append(result, ReplaceInjectChar(v))
	}
	if len(result) > 0 {
		return fmt.Sprintf("%s IN (%s)", field, strings.Join(result, ",")), true
	} else {
		return ReplaceInjectChar(val), true
	}
}

func GetBetween(field string, val string, m []string, spliter string) (string, bool) {
	arr := strings.Split(m[1], ",")
	result := make([]string, 0)
	isNot := ""
	if strings.HasPrefix(spliter, "!") {
		isNot = "NOT "
	}
	for _, v := range arr {
		if len(strings.TrimSpace(v)) == 0 {
			continue
		}
		arr2 := strings.Split(v, spliter)
		if len(arr2) == 2 && strings.TrimSpace(arr2[0]) != "" && strings.TrimSpace(arr2[1]) != "" {
			result = append(result, fmt.Sprintf("%s %sBETWEEN '%s' AND '%s'", field, isNot, ReplaceInjectChar(arr2[0]), ReplaceInjectChar(arr2[1])))
		}
	}
	if len(result) > 0 {
		return strings.Join(result, " OR "), true
	} else {
		return ReplaceInjectChar(val), true
	}
	return "", false
}

func FieldCaseTo(field string) string {
	if config.Config.FieldCase == "UCamel" {
		return jsonUtil.Ucfirst(jsonUtil.Case2Camel(field))
	} else if config.Config.FieldCase == "LCamel" {
		return jsonUtil.Lcfirst(jsonUtil.Case2Camel(field))
	}
	return jsonUtil.Camel2Case(field)
}

func ReplaceInjectChar(value string) string {
	value = strings.ReplaceAll(value, "'", "’")
	value = strings.ReplaceAll(value, "\"", "“")
	value = strings.ReplaceAll(value, "`", "·")
	value = strings.ReplaceAll(value, ",", "，")
	return value
}

func TrimInjectChar(value string) string {
	value = strings.ReplaceAll(value, "'", "")
	value = strings.ReplaceAll(value, "\"", "")
	value = strings.ReplaceAll(value, "`", "")
	value = strings.ReplaceAll(value, ",", "")
	return value
}
