package internal

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/ccait-dev/fast-api/fast/config"
	"gitee.com/ccait-dev/fast-api/fast/database"
	"gitee.com/ccait-dev/fast-api/fast/database/entity"
	"gitee.com/ccait-dev/fast-api/fast/database/express"
	"gitee.com/ccait-dev/fast-api/fast/types"
	"gitee.com/ccait-dev/fast-api/fast/utils/encryptUtil"
	"gitee.com/ccait-dev/fast-api/fast/utils/excelUtil"
	"gitee.com/ccait-dev/fast-api/fast/utils/fileUtil"
	"gitee.com/ccait-dev/fast-api/fast/utils/idUtil"
	"gitee.com/ccait-dev/fast-api/fast/utils/jsonUtil"
	"gitee.com/ccait-dev/fast-api/fast/utils/requestUtil"
	"github.com/gin-gonic/gin"
	"github.com/tealeg/xlsx"
	"gorm.io/gorm"
	"io"
	"net/http"
	"reflect"
	"strings"
	"time"
)

var (
	root = "./uploads"
)

type Handles struct {
	handleMap types.HandleMap
}

func init() {
	if strings.TrimSpace(config.Config.UploadRootPath) != "" {
		root = config.Config.UploadRootPath
	}
}

func NewHandles(hMap types.HandleMap) *Handles {
	return &Handles{
		handleMap: hMap,
	}
}

func Export() types.HandleMap {
	return types.HandleMap{
		"create": func(c *gin.Context, params map[string]interface{}, db *gorm.DB, opts *types.Options) {
			table := express.TrimInjectChar(c.Param("table"))
			if len(strings.TrimSpace(table)) == 0 {
				c.JSON(http.StatusBadRequest, entity.ResponseData{
					Message: "param table can not be empty",
					ErrCode: http.StatusBadRequest,
				})
			}
			for key, value := range params {
				filename := fmt.Sprintf("filename_%s", key)
				if opts.OnUpload != nil {

					if isByteArray(value) {
						success, path := opts.OnUpload(c, filename, value.([]byte), config.Config)
						if success {
							params[key] = path
						}
					}

				} else {
					file := params[key].([]byte)
					if path, err := fileUtil.Upload(root, filename, file); err != nil {
						c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
							"message": "上传失败: " + err.Error(),
						})
						return
					} else {
						params[key] = path
					}
				}
			}

			if !jsonUtil.HasId(params) {
				params["id"] = idUtil.CreateId()
			}
			helper := database.NewDBHelperByTable(table)
			helper.Insert(db.Table(table), params)
			c.JSON(http.StatusOK, entity.ResponseData{
				Message: "ok",
			})
		},

		"update": func(c *gin.Context, params map[string]interface{}, db *gorm.DB, opts *types.Options) {
			table := express.TrimInjectChar(c.Param("table"))
			if len(strings.TrimSpace(table)) == 0 {
				c.JSON(http.StatusBadRequest, entity.ResponseData{
					Message: "param table can not be empty",
					ErrCode: http.StatusBadRequest,
				})
				return
			}

			if opts.OnUpload != nil {
				for key, value := range params {
					if isByteArray(value) {
						success, path := opts.OnUpload(c, fmt.Sprintf("filename_%s", key), value.([]byte), config.Config)
						if success {
							params[key] = path
						}
					}
				}
			}

			helper := database.NewDBHelperByTable(table)
			var id any = ""
			if jsonUtil.HasId(params) {
				id = params["id"]
				delete(params, "id")
			} else {
				c.JSON(http.StatusBadRequest, entity.ResponseData{
					Message: "id field can not be empty",
					ErrCode: http.StatusBadRequest,
				})
				return
			}
			helper.Update(db.Table(table), id, params)
			c.JSON(http.StatusOK, entity.ResponseData{
				Message: "ok",
			})
		},

		"delete": func(c *gin.Context, params map[string]interface{}, db *gorm.DB, opts *types.Options) {
			table := express.TrimInjectChar(c.Param("table"))
			if len(strings.TrimSpace(table)) == 0 {
				c.JSON(http.StatusBadRequest, entity.ResponseData{
					Message: "param table can not be empty",
					ErrCode: http.StatusBadRequest,
				})
			}
			helper := database.NewDBHelperByTable(table)
			if _, exits := params["id"]; !exits {
				c.JSON(http.StatusBadRequest, entity.ResponseData{
					Message: "id field can not be empty",
					ErrCode: http.StatusBadRequest,
				})
			}
			helper.Delete(db.Table(table), params)
			c.JSON(http.StatusOK, entity.ResponseData{
				Message: "ok",
			})
		},

		"read": func(c *gin.Context, params map[string]interface{}, db *gorm.DB, opts *types.Options) {
			table := express.TrimInjectChar(c.Param("table"))
			if len(strings.TrimSpace(table)) == 0 {
				c.JSON(http.StatusBadRequest, entity.ResponseData{
					Message: "param table can not be empty",
					ErrCode: http.StatusBadRequest,
				})
			}
			helper := database.NewDBHelperByTable(table)

			var result entity.Result
			queryInfo := requestUtil.GetQueryInfo(params)
			if queryInfo.Query == nil {
				result = helper.Find(db.Table(table), params, nil, nil)
			} else {
				result = queryData(queryInfo, result, helper, db.Table(table), params)
			}
			c.JSON(http.StatusOK, entity.ResponseData{
				Message: "ok",
				Result:  result,
			})
		},

		"read-join": func(c *gin.Context, params map[string]interface{}, db *gorm.DB, opts *types.Options) {
			var result entity.Result
			queryInfo := requestUtil.GetQueryInfo(params)
			if queryInfo.Query == nil || len(queryInfo.Joins) == 0 {
				c.JSON(http.StatusInternalServerError, entity.ResponseData{
					Message: "Params Parse Error",
					Result:  result,
				})
				return
			}

			sb := &strings.Builder{}
			for i, join := range queryInfo.Joins {
				if i == 0 && strings.TrimSpace(join.Table) == "" {
					c.JSON(http.StatusInternalServerError, entity.ResponseData{
						Message: "Not Found First Table For Joins!",
						Result:  result,
					})
					return
				}
				if strings.TrimSpace(join.Table) == "" {
					continue
				}
				if i == 0 {
					db = db.Table(express.TrimInjectChar(join.Table))
				} else {
					joinMode := strings.ToLower(join.JoinMode)
					switch joinMode {
					case "left":
						sb.WriteString(fmt.Sprintf(" LEFT JOIN %s ", express.TrimInjectChar(join.Table)))
						break
					case "right":
						sb.WriteString(fmt.Sprintf(" RIGHT JOIN %s ", express.TrimInjectChar(join.Table)))
						break
					default:
						sb.WriteString(fmt.Sprintf(" INNER JOIN %s ", express.TrimInjectChar(join.Table)))
						break
					}
					if strings.TrimSpace(join.Alias) != "" {
						sb.WriteString(fmt.Sprintf(" AS %s", express.TrimInjectChar(join.Alias)))
					}
					if len(join.OnList) > 0 {
						for j, on := range join.OnList {
							if strings.TrimSpace(on.Name) == "" ||
								strings.TrimSpace(on.Value) == "" ||
								strings.TrimSpace(on.Algorithm) == "" {
								continue
							}
							if j == 0 {
								sb.WriteString(fmt.Sprintf(" ON %s%s%s", express.TrimInjectChar(on.Name), express.TrimInjectChar(on.Algorithm), express.TrimInjectChar(on.Value)))
							} else {
								sb.WriteString(fmt.Sprintf(" AND %s%s%s", express.TrimInjectChar(on.Name), express.TrimInjectChar(on.Algorithm), express.TrimInjectChar(on.Value)))
							}
						}
					}
				}
			}
			if sb.Len() > 0 {
				db = db.Joins(sb.String())
			}

			helper := database.NewDBHelper()
			result = queryData(queryInfo, result, helper, db, params)

			c.JSON(http.StatusOK, entity.ResponseData{
				Message: "ok",
				Result:  result,
			})
		},

		"download": func(c *gin.Context, params map[string]interface{}, db *gorm.DB, opts *types.Options) {
			table := express.TrimInjectChar(c.Param("table"))
			field := express.TrimInjectChar(c.Param("field"))
			id := express.TrimInjectChar(c.Param("id"))
			if len(strings.TrimSpace(table)) == 0 ||
				len(strings.TrimSpace(field)) == 0 ||
				len(strings.TrimSpace(id)) == 0 {
				c.JSON(http.StatusBadRequest, entity.ResponseData{
					Message: "param can not be empty",
					ErrCode: http.StatusBadRequest,
				})
			}
			helper := database.NewDBHelperByTable(table)
			params = map[string]interface{}{
				"id": express.TrimInjectChar(id),
			}
			result := helper.First(db.Table(table).Select(fmt.Sprintf("filename, %s", field)), params, nil, nil)
			data := result.Data.(map[string]interface{})
			if opts.OnDownload != nil {
				success := opts.OnDownload(c, data["fileName"].(string), data[field], config.Config)
				if !success {
					c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
						"message": "下载失败",
					})
				}
				return
			} else {
				b, filename, contentType := fileUtil.ReadFile(root, data[field].(string))
				reader := bytes.NewReader(b)
				c.Header("Content-Disposition", fmt.Sprintf("attachment; filename=%s", filename))
				c.Header("Content-Type", contentType) // Set Content-Type to audio/mpeg
				io.Copy(c.Writer, reader)
			}
		},

		"import": func(c *gin.Context, params map[string]interface{}, db *gorm.DB, opts *types.Options) {
			for _, value := range params {
				if isByteArray(value) {
					file, ok := value.([]byte)
					if !ok {
						c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
							"message": "文件格式有误",
							"status":  http.StatusInternalServerError,
						})
						return
					}
					excel, err := xlsx.OpenBinary(file)
					if err != nil {
						c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
							"message": err.Error(),
							"status":  http.StatusInternalServerError,
						})
						return
					}
					strs, ok := params["columns"]
					if !ok || strs == nil || len(strs.([]string)) == 0 {
						c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
							"message": "无效的导入格式",
							"status":  http.StatusInternalServerError,
						})
						return
					}
					var columns types.ColumnInfos
					json.Unmarshal([]byte(strs.([]string)[0]), &columns)
					if data, err := excelUtil.GetDataFromSheet(excel.Sheets[0], 1, columns); err == nil {
						if err != nil {
							c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
								"message": err.Error(),
								"status":  http.StatusInternalServerError,
							})
							return
						}

						table, ok := c.Params.Get("table")
						helper := database.NewDBHelperByTable(table)
						dict := helper.QueryDict(db, columns)
						if !ok {
							c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
								"message": "Invalid Request",
							})
							return
						}
						table = express.TrimInjectChar(table)
						for _, datum := range data {
							datum["id"] = idUtil.CreateId()
							if columns.HasField("createdAt") {
								datum["createdAt"] = time.Now()
							}
							if columns.HasField("createdBy") {
								tokenInfo := requestUtil.GetToken(c, config.Config.UserSigningKey)
								if tokenInfo == nil {
									return
								}
								if id, ok := tokenInfo["id"]; !ok {
									c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Timeout!!!"})
									return
								} else {
									datum["createdBy"] = id
								}
							}
							hasErr := false
							for _, column := range columns {
								if column.IgnoreField {
									delete(datum, column.Field)
									continue
								}
								if column.Md5Encrypt && columns.HasField(column.Field) {
									datum[column.Field] = encryptUtil.Md5(datum[column.Field].(string) + config.Config.UserSigningKey)
								}
								if !column.DictQuery.Empty() {
									if d, ok := dict[column.DictQuery.Table]; ok && d != nil {
										if v, ok := d[datum[column.Field]]; ok {
											datum[column.Field] = v
										} else if column.Require {
											hasErr = true
										}
									} else if column.Require {
										hasErr = true
									}
									if strings.TrimSpace(column.DictQuery.ReplaceField) != "" {
										datum[column.DictQuery.ReplaceField] = datum[column.Field]
										delete(datum, column.Field)
									}
								}
							}
							if hasErr {
								c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
									"message": "导入失败：必填项有误",
									"status":  http.StatusInternalServerError,
								})
								return
							}
							dataMap := make(map[string]interface{})
							for key, val := range datum {
								dataMap[jsonUtil.Camel2Case(key)] = val
							}
							db = db.Table(jsonUtil.Camel2Case(table)).Create(&dataMap)
							if db.Error != nil {
								c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
									"message": fmt.Sprintf("导入失败"),
								})
								return
							}
						}
						c.AbortWithStatusJSON(http.StatusOK, gin.H{
							"message": "导入成功",
						})
						return
					}
				}
			}
		},
	}
}

func queryData(queryInfo entity.Query, result entity.Result, helper *database.DBHelper, db *gorm.DB, params map[string]interface{}) entity.Result {
	if queryInfo.Count {
		result = entity.Result{
			Data: helper.Count(db, params),
		}
	} else if queryInfo.Export {
		result = helper.Find(db, queryInfo.Query, queryInfo.OrderBy, queryInfo.Select)
		//TODO export excel
	} else if queryInfo.Page.PageIndex > 0 && queryInfo.Page.PageSize > 0 {
		result = helper.Page(db, queryInfo.Query, queryInfo.Page, queryInfo.OrderBy, queryInfo.Select)
	}
	return result
}

func isArray(value any) bool {
	return reflect.TypeOf(value).Kind() == reflect.Array || reflect.TypeOf(value).Kind() == reflect.Slice
}

func isByteArray(value interface{}) bool {
	return reflect.TypeOf(value).Kind() == reflect.Slice && reflect.TypeOf(value).Elem().Kind() == reflect.Uint8
}

func (h Handles) IsFastApi(path string) bool {
	if config.Config.Fast == nil {
		return false
	}
	if len(strings.TrimSpace(path)) == 0 {
		return false
	}
	if isArray(config.Config.Fast) {
		arr := reflect.ValueOf(config.Config.Fast).Interface().([]string)
		if len(arr) > 0 {
			if strings.HasPrefix(path, config.Config.FastGroup+"/") {
				return true
			}
		}
	} else {
		isFast := reflect.ValueOf(config.Config.Fast).Interface().(bool)
		if isFast {
			if strings.HasPrefix(path, config.Config.FastGroup+"/") {
				return true
			}
		}
	}
	return false
}

func (h Handles) GetHandler(c *gin.Context, fieldCase string) (types.HandleFunc, map[string]interface{}, bool) {
	path := c.FullPath()
	key := path
	if h.IsFastApi(path) {
		arr := strings.Split(path, "/")
		key = strings.ToLower(arr[len(arr)-1])
	} else {
		for _, routeInfo := range config.Config.Routes {
			if routeInfo.Url == path {
				if len(strings.TrimSpace(routeInfo.Handler)) == 0 {
					key = routeInfo.Url
				} else {
					key = routeInfo.Handler
				}
				break
			}
		}
	}
	_, exists := h.handleMap[key]
	if !exists {
		http.NotFound(c.Writer, c.Request)
		return nil, nil, false
	}
	var params = requestUtil.GetData(c, fieldCase)
	switch key {
	case "create":
		if config.Config.DisableSnowflakeId {
			delete(params, "id")
		} else if _, exists := params["id"]; exists {
			params["id"] = idUtil.CreateId()
		}
		break
	case "read":
		break
	}
	fn := h.handleMap[key]
	return fn, params, true
}
