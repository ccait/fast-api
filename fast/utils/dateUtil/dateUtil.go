package dateUtil

import (
	"gitee.com/ccait-dev/fast-api/fast/utils/loggerUtil"
	"time"
)

func Parse(str interface{}, defaultTime time.Time) *time.Time {
	if str == nil {
		return &defaultTime
	}
	timeStr, ok := str.(string)
	if !ok {
		return &defaultTime
	}
	if timeStr == "" {
		return &defaultTime
	}
	// 解析时间字符串
	t, err := time.Parse(time.RFC3339, timeStr)
	if err != nil {
		loggerUtil.Error("Error parsing time:", err)
		return &defaultTime
	}
	return &t
}
