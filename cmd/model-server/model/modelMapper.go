package model

func GetModelMapper() map[string]interface{} {
	var mapper = make(map[string]interface{})
	registerMapper(mapper)
	return mapper
}

func registerMapper(mapper map[string]interface{}) {
	mapper["accounts"] = Accounts{}
}
