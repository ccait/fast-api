package jsonUtil

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitee.com/ccait-dev/fast-api/fast/utils/loggerUtil"
	"io"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"unicode"
)

// 下划线json
type JsonSnakeCase struct {
	Value interface{}
}

func (c JsonSnakeCase) MarshalJSON() ([]byte, error) {
	// Regexp definitions
	var keyMatchRegex = regexp.MustCompile(`\"(\w+)\"\s*:`)
	var wordBarrierRegex = regexp.MustCompile(`(\w)([A-Z])`)
	marshalled, err := json.Marshal(c.Value)
	converted := keyMatchRegex.ReplaceAllFunc(
		marshalled,
		func(match []byte) []byte {
			return bytes.ToLower(wordBarrierRegex.ReplaceAll(
				match,
				[]byte(`${1}_${2}`),
			))
		},
	)
	return converted, err
}

// 小驼峰json
type JsonCamelCaseLcfirst struct {
	Value interface{}
}

// 大驼峰json
type JsonCamelCaseUcfirst struct {
	Value interface{}
}

func (c JsonCamelCaseLcfirst) MarshalJSON() ([]byte, error) {
	var keyMatchRegex = regexp.MustCompile(`\"(\w+)\"\s*:`)
	marshalled, err := json.Marshal(c.Value)
	converted := keyMatchRegex.ReplaceAllFunc(
		marshalled,
		func(match []byte) []byte {
			matchStr := string(match)
			key := matchStr[1 : len(matchStr)-2]
			resKey := Lcfirst(Case2Camel(key))
			return []byte(`"` + resKey + `":`)
		},
	)
	return converted, err
}

func (c JsonCamelCaseUcfirst) MarshalJSON() ([]byte, error) {
	var keyMatchRegex = regexp.MustCompile(`\"(\w+)\"\s*:`)
	marshalled, err := json.Marshal(c.Value)
	converted := keyMatchRegex.ReplaceAllFunc(
		marshalled,
		func(match []byte) []byte {
			matchStr := string(match)
			key := matchStr[1 : len(matchStr)-2]
			resKey := Ucfirst(Case2Camel(key))
			return []byte(`"` + resKey + `":`)
		},
	)
	return converted, err
}

// 驼峰式写法转为下划线写法
func Camel2Case(name string) string {
	buffer := NewBuffer()
	for i, r := range name {
		if unicode.IsUpper(r) {
			if i != 0 {
				buffer.Append('_')
			}
			buffer.Append(unicode.ToLower(r))
		} else {
			buffer.Append(r)
		}
	}
	return buffer.String()
}

// 下划线写法转为驼峰写法
func Case2Camel(name string) string {
	name = strings.Replace(name, "_", " ", -1)
	name = strings.Title(name)
	return strings.Replace(name, " ", "", -1)
}

// 首字母大写
func Ucfirst(str string) string {
	for i, v := range str {
		return string(unicode.ToUpper(v)) + str[i+1:]
	}
	return ""
}

// 首字母小写
func Lcfirst(str string) string {
	for i, v := range str {
		return string(unicode.ToLower(v)) + str[i+1:]
	}
	return ""
}

// 内嵌bytes.Buffer，支持连写
type Buffer struct {
	*bytes.Buffer
}

func NewBuffer() *Buffer {
	return &Buffer{Buffer: new(bytes.Buffer)}
}

func (b *Buffer) Append(i interface{}) *Buffer {
	switch val := i.(type) {
	case int:
		b.append(strconv.Itoa(val))
	case int64:
		b.append(strconv.FormatInt(val, 10))
	case uint:
		b.append(strconv.FormatUint(uint64(val), 10))
	case uint64:
		b.append(strconv.FormatUint(val, 10))
	case string:
		b.append(val)
	case []byte:
		b.Write(val)
	case rune:
		b.WriteRune(val)
	}
	return b
}

func (b *Buffer) append(s string) *Buffer {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("***** jsonUtil: 内存不够了！******")
		}
	}()
	b.WriteString(s)
	return b
}

func ParseData(jsonStr string) map[string]interface{} {
	return ToData([]byte(jsonStr))
}

func GetData(reader io.Reader) (map[string]interface{}, error) {
	reslut := make(map[string]interface{})
	// 解析JSON到interface{}
	if err := json.NewDecoder(reader).Decode(&reslut); err != nil {
		return nil, err
	}
	return reslut, nil
}

func ToData(jsonStr []byte) map[string]interface{} {
	result := make(map[string]interface{})
	if !IsValidJSON(string(jsonStr)) {
		return result
	}
	// 将[]byte转换为io.Reader
	reader := bytes.NewReader(jsonStr)
	if err := json.NewDecoder(reader).Decode(&result); err != nil {
		loggerUtil.Error(err.Error())
		return nil
	}
	return result
}

func ToJson(data interface{}) string {
	jsonData, err := json.Marshal(data)
	if err != nil {
		fmt.Printf("JSON marshaling failed: %s\n", err.Error())
	}
	jsonString := string(jsonData)

	return jsonString
}

func ToStruct(m map[string]interface{}, result *interface{}) *interface{} {
	err := json.Unmarshal([]byte(ToJson(m)), &result)
	if err != nil {
		loggerUtil.Error(err.Error())
	}
	return result
}

func ParseLcCamelCase(bytes []byte) map[string]interface{} {
	return ParseCamelCase(bytes, false)
}

func ParseUcCamelCase(bytes []byte) map[string]interface{} {
	return ParseCamelCase(bytes, true)
}

func ParseCamelCase(bytes []byte, ucfirst bool) map[string]interface{} {
	return CamelCase(ToData(bytes), ucfirst)
}

func CamelCase(obj interface{}, ucfirst bool) map[string]interface{} {
	if ucfirst {
		jsonCase := JsonCamelCaseUcfirst{obj}
		data, err := jsonCase.MarshalJSON()
		if err != nil {
			loggerUtil.Error(err.Error())
			return nil
		}
		return ToData(data)
	} else {
		jsonCase := JsonCamelCaseLcfirst{obj}
		data, err := jsonCase.MarshalJSON()
		if err != nil {
			loggerUtil.Error(err.Error())
			return nil
		}
		return ToData(data)
	}
}

func ParseSnakeCase(bytes []byte) map[string]interface{} {
	return SnakeCase(ToData(bytes))
}

func SnakeCase(obj interface{}) map[string]interface{} {
	jsonCase := JsonSnakeCase{obj}
	data, err := jsonCase.MarshalJSON()
	if err != nil {
		loggerUtil.Error(err.Error())
		return nil
	}
	return ToData(data)
}

func ContainsKey(obj map[string]interface{}, key string) bool {
	_, exists := obj[key]
	return exists
}

func HasId(obj map[string]interface{}) bool {
	if ContainsKey(obj, "id") {
		if val, is := IsNumber(obj["id"]); is {
			if val > 0 {
				return true
			}
		} else if IsString(obj["id"]) {
			val := reflect.ValueOf(obj["id"])
			if len(strings.TrimSpace(val.String())) > 0 {
				return true
			}
		} else if IsArray(obj["id"]) {
			val := reflect.ValueOf(obj["id"])
			if val.Len() > 0 {
				return true
			}
		}
	}
	return false
}

func IsString(obj interface{}) bool {
	return reflect.TypeOf(obj).Kind() == reflect.String
}

func IsArray(obj interface{}) bool {
	return reflect.TypeOf(obj).Kind() == reflect.Array ||
		reflect.TypeOf(obj).Kind() == reflect.Slice
}

func IsNumber(obj interface{}) (float64, bool) {
	if reflect.TypeOf(obj).Kind() == reflect.Float64 {
		return reflect.ValueOf(obj).Float(), true
	}
	if reflect.TypeOf(obj).Kind() == reflect.Float32 {
		return reflect.ValueOf(obj).Float(), true
	}
	if reflect.TypeOf(obj).Kind() == reflect.Int64 {
		return float64(reflect.ValueOf(obj).Int()), true
	}
	if reflect.TypeOf(obj).Kind() == reflect.Int16 {
		return float64(reflect.ValueOf(obj).Int()), true
	}
	if reflect.TypeOf(obj).Kind() == reflect.Int32 {
		return float64(reflect.ValueOf(obj).Int()), true
	}
	if reflect.TypeOf(obj).Kind() == reflect.Int64 {
		return float64(reflect.ValueOf(obj).Int()), true
	}
	return 0, false
}

func IsValidJSON(str string) bool {
	var js json.RawMessage
	return json.Unmarshal([]byte(str), &js) == nil
}
