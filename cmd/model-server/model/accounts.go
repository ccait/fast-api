package model

import (
	"gitee.com/ccait-dev/fast-api/fast/database/entity"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"log"
	"time"
)

type Accounts struct {
	ID        uint64     `gorm:"column:id;type:bigint(20);primary_key" json:"id"`
	Account   string     `gorm:"column:account;type:varchar(255)" json:"account"`    // 账号
	Password  string     `gorm:"column:password;type:varchar(255)" json:"password"`  // 密碼僅支持大小寫字母+數字
	Nickname  string     `gorm:"column:nickname;type:varchar(32)" json:"nickname"`   // 昵称
	Realname  string     `gorm:"column:realname;type:varchar(32)" json:"realname"`   // 真实姓名
	Gender    string     `gorm:"column:gender;type:varchar(8)" json:"gender"`        // lady=女，gent=男，none=未知
	Mail      string     `gorm:"column:mail;type:varchar(64)" json:"mail"`           // 邮箱
	CreatedAt *time.Time `gorm:"column:created_at;type:datetime" json:"createdAt"`   // 创建时间
	CreatedBy int64      `gorm:"column:created_by;type:bigint(20)" json:"createdBy"` // 创建人
	UpdatedBy int64      `gorm:"column:updated_by;type:bigint(20)" json:"updatedBy"` // 修改人
	UpdatedAt *time.Time `gorm:"column:updated_at;type:datetime" json:"updatedAt"`   // 修改时间
	Status    string     `gorm:"column:status;type:varchar(8)" json:"status"`        // 状态：enabled=启用，disabled=禁用，deleted=删除
	Strong    int        `gorm:"column:strong;type:smallint(6)" json:"strong"`       // 密码强度
}

func (model Accounts) Login(ctx *gin.Context, params map[string]interface{}, db *gorm.DB) {
	var msg = "this is login"
	log.Println(msg)
	ctx.JSON(200, entity.ResponseData{Message: msg})
}
