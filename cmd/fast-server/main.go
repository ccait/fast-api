package main

import (
	"fmt"
	"gitee.com/ccait-dev/fast-api/fast"
	"gitee.com/ccait-dev/fast-api/fast/config"
	"gitee.com/ccait-dev/fast-api/fast/database/entity"
	. "gitee.com/ccait-dev/fast-api/fast/types"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"log"
)

func main() {
	fast.BuildServer(&Options{
		Handles: HandleMap{
			"LoginHandler": func(ctx *gin.Context, params map[string]interface{}, db *gorm.DB, opts *Options) {
				var msg = "this is login"
				log.Println(msg)
				ctx.JSON(200, entity.ResponseData{Message: msg})
			},
			"LogoutHandler": func(ctx *gin.Context, params map[string]interface{}, db *gorm.DB, opts *Options) {
				var msg = "this is logout"
				log.Println(msg)
				ctx.JSON(200, entity.ResponseData{Message: msg})
			},
		},
		OnBeforeStart: func(eng *gin.Engine, config *config.AppConfig) {
			eng.Use(func(context *gin.Context) {
				fmt.Printf("Handle-----> ")
			})
		},
	})
}
