package fast

import (
	"fmt"
	"gitee.com/ccait-dev/fast-api/fast/config"
	"gitee.com/ccait-dev/fast-api/fast/database"
	"gitee.com/ccait-dev/fast-api/fast/types"
	"gitee.com/ccait-dev/fast-api/fast/utils/jsonUtil"
	"gitee.com/ccait-dev/fast-api/internal"
	"github.com/gin-gonic/gin"
	"github.com/logrusorgru/aurora"
	"gorm.io/gorm"
	"net/http"
	"os"
	"reflect"
	"strings"
)

var (
	appName       string
	appConfig     *config.AppConfig
	fastHandleMap types.HandleMap
)

type Server struct {
	Opts types.Options
}

func init() {
	fastHandleMap = internal.Export()
	appConfig = config.Config
	appName = "fast-api"
}

func BuildServer(opt *types.Options) {
	if opt == nil {
		opt = new(types.Options)
	}
	server := Server{}
	server.Opts = *opt
	server.Start(opt)
	if opt.DisableFastApi {
		appConfig.Fast = false
	}
}

func (*Server) Start(opt *types.Options) {
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)
	go func() {
		<-sigs
		done <- true
	}()

	// 设置Gin为发布模式
	gin.SetMode(gin.ReleaseMode)
	// 创建一个Gin路由器
	r := gin.Default()
	fmt.Println(fmt.Sprintf("%s before start.", appName))
	configServer(r)
	fmt.Println(fmt.Sprintf("%s config server.", appName))
	if opt.OnBeforeStart != nil {
		opt.OnBeforeStart(r, appConfig)
		fmt.Sprintf("%s invoke OnBeforeStart event.", appName)
	}
	r.GET("/fast-api/testing", func(c *gin.Context) {
		c.JSON(200, gin.H{"msg": "success"})
	})
	fmt.Println(fmt.Sprintf("%s register: /fast-api/testing", appName))
	//注册Api
	registerApi(opt, r)
	fmt.Println(fmt.Sprintf("%s register api by options.", appName))
	// 启动服务器，不填入参默认在0.0.0.0:8080上监听
	if len(strings.TrimSpace(appConfig.Port)) == 0 {
		appConfig.Port = "8080"
	}
	fmt.Println(aurora.Cyan(fmt.Sprintf("%s begin listing port: %s.\n", appName, aurora.Magenta(appConfig.Port))))
	fmt.Println(aurora.Cyan(aurora.BgMagenta(fmt.Sprintf("Welcome use %s! Server has been running......", appName))))
	r.Run("0.0.0.0:" + string(appConfig.Port))

	<-done
	fmt.Println(aurora.BgRed("  Closed  "))
	fmt.Println(aurora.BgGreen("  Finished  "))
}

func configServer(r *gin.Engine) {
	// 创建一个日志中间件，记录请求的基本信息
	r.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		return fmt.Sprintf("[%s] \"%s %s %s\" %d %d\n",
			param.TimeStamp.Format("2006/01/02 - 15:04:05"),
			param.Request.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.BodySize,
		)
	}))

	// 跨域中间件
	r.Use(func(c *gin.Context) {
		if len(strings.TrimSpace(appConfig.Methods)) == 0 {
			appConfig.Methods = "GET, POST, PUT, DELETE, PATCH, OPTIONS"
		}

		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Methods", appConfig.Methods)
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}
		c.Next()
	})
}

func registerApi(opt *types.Options, r *gin.Engine) {
	var handles = types.HandleMap{}
	if !appConfig.DisableMapper && opt.ModelMapper != nil {
		fillHandlesByModels(appConfig, opt.ModelMapper, handles)
	}

	if opt.Handles != nil && len(opt.Handles) > 0 {
		for key, value := range opt.Handles {
			handles[key] = value
		}
	}

	// 在路由器上添加GET请求处理
	for _, route := range appConfig.Routes {
		if len(strings.TrimSpace(route.Url)) == 0 {
			continue
		}
		if !strings.HasPrefix(route.Url, "/") {
			continue
		}
		if isFastApi(route.Url) {
			continue
		}
		if len(strings.TrimSpace(route.Handler)) == 0 {
			route.Handler = route.Url
		}
		if handles[route.Handler] == nil {
			continue
		}
		customHandler := func(c *gin.Context) {
			handler, params, exists := internal.NewHandles(handles).GetHandler(c, "LCamel")
			if !exists {
				return
			}
			factory := database.DBFactory{}
			dbname := c.Param("db")
			db, err := factory.GetDB(dbname)
			if err != nil {
				c.JSON(404, gin.H{"message": "404"})
				http.NotFoundHandler()
				return
			}
			handler(c, params, db, opt)
		}
		setRouter(customHandler, route.Url, route.Method, r, "/")
	}

	if isEnabledFastApi() {
		// 创建一个回调函数，用于处理请求
		fastHandler := func(c *gin.Context) {
			handler, params, exists := internal.NewHandles(fastHandleMap).GetHandler(c, "")
			if !exists {
				return
			}
			factory := database.DBFactory{}
			dbname := c.Param("db")
			db, err := factory.GetDB(dbname)
			if err != nil {
				c.JSON(404, gin.H{"message": "404"})
				http.NotFoundHandler()
				return
			}
			handler(c, params, db, opt)
		}

		if len(strings.TrimSpace(appConfig.FastGroup)) == 0 {
			appConfig.FastGroup = "/fast-api"
		}

		if isArray(appConfig.Fast) {
			// 将对象转换为数组
			arr := reflect.ValueOf(appConfig.Fast).Interface().([]string)
			for _, dbname := range arr {
				for _, routeInfo := range internal.ROUTES {
					setRouter(fastHandler, strings.Replace(routeInfo.Path, "/:db/", "/"+dbname+"/", -1), routeInfo.Method, r, appConfig.FastGroup)
				}
			}
		} else {
			for _, routeInfo := range internal.ROUTES {
				setRouter(fastHandler, routeInfo.Path, routeInfo.Method, r, appConfig.FastGroup)
			}
		}
	}
}

func setRouter(handler gin.HandlerFunc, path string, method string, r *gin.Engine, prefix string) {
	notFoundHandler := func(c *gin.Context) {
		c.JSON(404, gin.H{"message": "404"})
		http.NotFoundHandler()
	}
	if len(strings.TrimSpace(prefix)) > 0 && prefix != "/" && !strings.HasPrefix(path, prefix) {
		path = prefix + path
	}
	switch method {
	case "GET":
		r.GET(path, handler)
		break
	case "POST":
		r.POST(path, handler)
		break
	case "PUT":
		r.PUT(path, handler)
		break
	case "DELETE":
		r.DELETE(path, handler)
		break
	case "PATCH":
		r.PATCH(path, handler)
		break
	case "OPTIONS":
		r.OPTIONS(path, handler)
		break
	default:
		r.NoRoute(notFoundHandler)
		break
	}
}

func isArray(value any) bool {
	return reflect.TypeOf(value).Kind() == reflect.Array
}

func isEnabledFastApi() bool {
	if reflect.TypeOf(appConfig.Fast).Kind() == reflect.Bool {
		return true
	}
	if reflect.TypeOf(appConfig.Fast).Kind() == reflect.Array {
		// 将对象转换为数组
		arr := reflect.ValueOf(appConfig.Fast).Interface().([]string)
		if len(arr) > 0 {
			return true
		}
	}
	return false
}

func isFastApi(url string) bool {
	if isEnabledFastApi() {
		if strings.HasPrefix(url, "/api/fast/") {
			return true
		}
	}

	return false
}

func fillHandlesByModels(appConf *config.AppConfig, modelMapper map[string]interface{}, handleMap types.HandleMap) {
	for _, route := range appConf.Routes {
		if len(strings.TrimSpace(route.Url)) == 0 {
			continue
		}
		var uris = strings.Split(route.Url, "/")
		if len(uris) < 2 {
			continue
		}
		var table = jsonUtil.Lcfirst(jsonUtil.Case2Camel(uris[len(uris)-2]))
		var action = jsonUtil.Ucfirst(jsonUtil.Case2Camel(uris[len(uris)-1]))
		if _, exists := modelMapper[table]; !exists {
			continue
		}
		handleMap[route.Url] = func(ctx *gin.Context, params map[string]interface{}, db *gorm.DB, opts *types.Options) {
			model := reflect.ValueOf(modelMapper[table])
			method := model.MethodByName(action)

			// 确保是指针类型的方法
			if method.IsValid() {
				typ := method.Type()
				numIn := typ.NumIn()
				if numIn == 0 {
					// 调用方法，需要将指针值转换为方法的接收器类型
					method.Call([]reflect.Value{})
				} else if numIn == 1 {
					// 调用方法，需要将指针值转换为方法的接收器类型
					method.Call([]reflect.Value{reflect.ValueOf(ctx)})
				} else if numIn == 2 {
					// 调用方法，需要将指针值转换为方法的接收器类型
					method.Call([]reflect.Value{reflect.ValueOf(ctx), reflect.ValueOf(params)})
				} else if numIn == 3 {
					// 调用方法，需要将指针值转换为方法的接收器类型
					method.Call([]reflect.Value{reflect.ValueOf(ctx), reflect.ValueOf(params), reflect.ValueOf(db)})
				} else if numIn == 4 {
					// 调用方法，需要将指针值转换为方法的接收器类型
					method.Call([]reflect.Value{reflect.ValueOf(ctx), reflect.ValueOf(params), reflect.ValueOf(db), reflect.ValueOf(opts)})
				}
			}
		}
	}
}
