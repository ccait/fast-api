package internal

type RouteInfo struct {
	Method string
	Path   string
}

var (
	ROUTES []RouteInfo
)

func init() {
	if ROUTES == nil {
		ROUTES = make([]RouteInfo, 0)
	}
	ROUTES = append(ROUTES, RouteInfo{
		Path:   "/:db/:table/create",
		Method: "POST",
	}, RouteInfo{
		Path:   "/:db/:table/update",
		Method: "PUT",
	}, RouteInfo{
		Path:   "/:db/:table/read",
		Method: "GET",
	}, RouteInfo{
		Path:   "/:db/:table/delete",
		Method: "DELETE",
	}, RouteInfo{
		Path:   "/:db/read-join",
		Method: "GET",
	}, RouteInfo{
		Path:   "/:db/:table/import",
		Method: "PUT",
	}, RouteInfo{
		Path:   "/:db/:table/:field/:id/download",
		Method: "GET",
	})
}
