# 设置GOLANG环境变量
export GO111MODULE=on
export GOPROXY=https://goproxy.cn,direct
# 设置默认的编译器
GO=go

# 设置默认的构建环境变量
GOOS?=$(shell $(GO) env GOOS)
GOARCH?=$(shell $(GO) env GOARCH)

# 设置构建目标
BUILD_TARGETS?=darwin/amd64 linux/amd64 windows/amd64

.PHONY: all build clean run check cover lint docker help deps fmt default clean-mod
BIN_FILE=myapp

default: build

all: check build

# 格式化代码
fmt:
	go fmt ./...

#build:
#	@go build -o "${BIN_FILE}"

# 为不同平台构建二进制文件
build:
	@for target in $(BUILD_TARGETS); do \
		GOOS=$$(echo $$target | cut -f1 -d'/') \
		GOARCH=$$(echo $$target | cut -f2 -d'/') \
		$(GO) build -o "bin/myapp_$$GOOS-$$GOARCH" ./cmd/myapp; \
	done

# 安装项目依赖
deps:
	go mod download
	go mod tidy
clean:
	go clean
	rm --force "mapp.out"
clean-mod:
	go clean -modcache
test:
	go test
check:
	go fmt ./
	go vet ./
cover:
	go test -coverprofile xx.out
	go tool cover -html=xx.out
run:
	go run ./main.go
	#./"${BIN_FILE}"
lint:
	golangci-lint run --enable-all
docker:
	@docker build -t ccait/fast-api:latest .
help:
	@echo "make 格式化go代码 并编译生成二进制文件"
	@echo "make build 编译go代码生成二进制文件"
	@echo "make clean 清理中间目标文件"
	@echo "make test 执行测试case"
	@echo "make check 格式化go代码"
	@echo "make cover 检查测试覆盖率"
	@echo "make run 直接运行程序"
	@echo "make lint 执行代码检查"
	@echo "make docker 构建docker镜像"