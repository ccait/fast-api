package types

import (
	"gitee.com/ccait-dev/fast-api/fast/database/express"
	"strings"
)

type DictInfo struct {
	Table         string                  `json:"table"`
	IdField       string                  `json:"idField" json:"id_field"`
	NameField     string                  `json:"nameField" json:"name_field"`
	ReplaceField  string                  `json:"replaceField" json:"replace_field"`
	Conditions    []express.ConditionInfo `json:"conditions"`
	GroupByFields []string                `json:"groupByFields" json:"group_by_fields"`
}

type DictInfos []DictInfo

func (dict DictInfo) Empty() bool {
	if strings.TrimSpace(dict.Table) == "" ||
		strings.TrimSpace(dict.IdField) == "" ||
		strings.TrimSpace(dict.NameField) == "" {
		return true
	}
	return false
}
