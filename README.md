# fast-api

### 介绍
基于gin+gorm的极简api服务，真正意义上的轻量级开发框架，内置的api/fast路由组提供了基于 restful风格的默认CRUD操作数据表。

### 软件架构
gin+gorm,数据库支持mysql、sqlite、postgresql、sqlserver


### 开发环境
Go1.23.0

### 安装依赖
```shell
go mod download
go mod tidy
```

下载指定依赖包
```shell
go get gitee.com/ccait-dev/fast-api
```

如版本冲突可能还需要清理模块缓存
```shell
go clean -modcache
```

编译二进制文件
```shell
go build
```

运行二进制文件
```shell
sh ./fast-api
```

编译并运行源代码
```shell
 go run main.go
```

使用Makefile
```shell
make #格式化go代码 并编译生成二进制文件
make build #编译go代码生成二进制文件
make clean #清理中间目标文件
make test #执行测试case
make check #格式化go代码
make cover #检查测试覆盖率
make run #直接运行代码
make lint #执行代码检查
make docker #构建docker镜像
```


### 配置文件 /config/application.yml
#### 1. 开启极简 API
```yaml
api:
  port: #端口
  fast: true #开启
  fastGroup: /fast-api #fast-api的路径前辍，默认为/fast-api
  serverName: FastApi #服务名称，可选参数 
  methods: GET, POST, PUT, DELETE, OPTIONS #允许的请求方式，不填默认所有
  disableSnowflakeId: false #禁用雪花ID，默认启用 
  disableMapper: false #禁用Mapper模式，默认启用
```
或指定允许操作的表名为白名单
```yaml
api:
  fast: ["user", "logs"] #开启白名单模式
```

#### 2.  自定义路由
```yaml
api:
  routes:
    - url: /login #绝对路径
      method: POST #请求方式
      handler: LoginHandler #通过函数处理map的key选择处理函数
```
\* 注意：启用Mapper模式下无需填handler,路由url规则为倒数第二节为model名称，末节为定义映射的方法名。


* 启用Mapper模式下映射路由方法
```go
package model

import (
	"gitee.com/ccait-dev/fast-api/fast/database/entity"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"log"
)

/*
函数说明：函数名Login对应路由url的login
参数说明： @ctx 为gin的上下文
      @params 前端提交的参数
      @db 数据库操作对象
指针说明：(model Accounts)，Accounts为你的Model
*/
func (model Accounts) Login(ctx *gin.Context, params map[string]interface{}, db *gorm.DB) {
	var msg = "this is login"
	log.Println(msg)
	//这里使用gore.DB查询数据库
	//gorm.DB.Find(params)
	
	//gin.Context返回结果
	ctx.JSON(200, entity.ResponseData{Message: msg})
}
```
``` yaml
注意: 映射路由方法最好在model目录下新增一个文件定义，比如account.go下建一个accountDao.go写Action函数，这样的做好处是数据库表设计修改时使用工具生成的Model可以直接复盖掉原有的model文件。
```

#### 3.  设置数据源
```yaml
api:
  datasource:
    # prometheus： 支持配置prometheus
    config: #gorm的配置，具体选项参考gorm项目文档
      prepareStmt: true
      allowGlobalUpdate: true
      disableAutomaticPing: false
    pools: #数据库连接池配置
      maxOpenConns: 20
      maxIdleConns: 10
      connMaxLifetime: 60000
      connMaxIdleTime: 600000
    connections: #数据库连接设置
      databaseName: #数据源名，一般指库名
        driver: mysql #数据库类型，支持mysql、sqlite、postgresql、sqlserver
        connectString: username:password@tcp(localhost:3306)/dbname
```


###  启动fast-api服务
```go
package main
import (
    "gitee.com/ccait-dev/fast-api/fast"
	. "gitee.com/ccait-dev/fast-api/fast/types"
	"gitee.com/ccait-dev/fast-api/fast/database/entity"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func main() {
	server := fast.Server{}
	server.Start(types.Options{
		Handles: types.HandleMap{ /*** Handles为可选参数，指自定义路由的处理程序映身表，application.yml配置路由时填的handler就是这里的key。 ***/
			"LoginHandler": func(ctx *gin.Context, params map[string]interface{}, db *gorm.DB) {
				ctx.JSON(200, entity.ResponseData{Message: "this is login"})
			},
		},
		ModelMapper: make(map[string]interface{}), /*** 启用mapper模式时，路由Url倒数第二节为Model名称时会与ModelMapper中的key匹配并映身到相应的handler，注意key为Model的小驼峰规则，Url尾节为Action，即与Model绑定的func。 ***/
		OnBeforeStart: func(eng *gin.Engine, config *config.AppConfig) {}, /*** 启动事件，服务装配完毕在启动前会调用该勾子，可在此处增加自定义gin的全局勾子函数，如路由拦截、鉴权。 ***/
	})
}
```

###  更多gin用法
参考Gin文档：[https://gin-gonic.com/zh-cn/docs/]()


###  fast-api内置的CRUD 接口
```yaml
接口请求规则: /fast-api/:db/:table
注: db=数据库名，table=表名
```

#### 1. 新增数据
```yaml
接口地址: //${服务地址:端口}/fast-api/:db/:table/create
请求方式: POST
参数格式: json、form表单
注意: 提交字段为文件类型时，该表下必须要有${field}_filename字段,${field}表示对应的字段名
```

#### 2. 更新数据
```yaml
接口地址: //${服务地址:端口}/fast-api/:db/:table/update
请求方式: PUT
参数格式: json、form表单
注意: 提交字段为文件类型时，该表下必须要有${field}_filename字段,${field}表示对应的字段名
```

#### 3. 删除数据
```yaml
接口地址: //${服务地址:端口}/fast-api/:db/:table/delete
请求方式: DELETE
参数格式: json、form表单
json示例: {id: 123456} #或 {id: [123456]}
```

#### 4. 查询数据
```yaml
接口地址: //${服务地址:端口}/fast-api/:db/:table/read
请求方式:  GET
参数格式: json、url参数
json参数示例: {id: 123456}
url参数示例: id=123456
分页参数示例: {
  count: false, #是否只返回总数
  export: false, #是否导出Excel
  page: {
    pageSize: 20,
    pageIndex: 1,
  },
  query: [{ #查询条件
    id: 2000
  }],
  orderBy: {
    id: 'desc'
  }, 
  select: ['field1', 'field2'], #选择字段
  data: [] #返回数据
}
注意: 分页参数格式只支持json
返回数据: {
  result: {
    data: [...], #返回查询数据
    page: { pageSize, pageIndex, totalPages },
    total: 99999,
  },
  message: 'ok',
  errorCode: 0,
}
```
* 注意：多个查询条件之前默认关系为 AND，可修改配置项queryMode: OR改变，字段值匹配默认为=，更多的匹配方式可使用 EL 表达式

### El表达式查询条件
字段名以$符号开头表示值须按El表达式查询解析条件，值需以${}括起，括弧内对应的字段名同样须以$符号开头表示，否则被认为字符串，可以双$符号代表当前字段


#### 查询参数query支持El表达式，表达式写法如下所示：

* 条件查询表达式
```json
{
  "$name": "${$name = 字典,component=zjlj}", //条件匹配操作符可以用=、>、>=、<=、<>、!=、CONTAINS、BEGIN、END
  "$component": "${name CONTAINS 字典,component=kk}"
}
```
或
```json
{
  "$name": "${$$ END 字典,component=kk}" //此处$$等同于$name
}
```

* IN查询: ${1,2,3,4,5,...}
```json
{
  "$id: "${1,2,3,4,5}"
}
```

* 数字范围值查询: ${1-3,5-7,...}
```json
{
  "&id: "${1-3,5-7}"
}
```
* 时间范围值查询: ${2022/01/01 00:00:00 - 2022/01/01 01:00:00,...}

  注意：时间格式为 yyyy/MM/dd HH:mm:ss-yyyy/MM/dd HH:mm:ss 或 yyyy-MM-dd HH:mm:ss~yyyy-MM-dd HH:mm:ss

```json
{
  "$create_time: "${2022/01/01 00:00:00 - 2022/01/01 01:00:00}"
}
```
或
```json
{
  "$create_time: "${2022-01-01 00:00:00 ~ 2022-01-01 01:00:00}"
}
```

#### 5. 查询联表数据
```yaml
接口地址: //${服务地址:端口}/fast-api/:db/read-join
请求方式:  GET
参数格式: json、form表单
json参数示例: {id: 123456}
分页参数示例: {
  count: false, #是否只返回总数
  export: false, #是否导出Excel
  page: {
    pageSize: 20,
    pageIndex: 1,
  },
  query: [{
    id: 2000
  }],
  "joins": [{ #多表联查
    "table": "salary", #表名
    "alias": "a", #别名
    "joinMode": "inner" #联表方式
  }, {
    "table": "archives",
    "alias": "b",
    "joinMode": "Inner",
    "onList": [{
      "name": "b.id",
      "value": "a.archives_id",
      "algorithm": "EQ"
    }]
  }],
  orderBy: {
    id: 'desc'
  },
  select: ['a.field1', 'b.field2'], #选择字段
}
注意: 分页参数格式只支持json
```

#### 6. 导入数据
```yaml
接口地址: //${服务地址:端口}/fast-api/:db/:table/import
请求方式:  PUT
参数格式: form表单
json参数示例: {fieldName: blob}
```

#### 7. 下载文件
```yaml
接口地址: //${服务地址:端口}/fast-api/:db/:table/:field/download
请求方式:  GET
参数格式: json、form表单
json参数示例: {id: 123123}
```


###  GORM操作数据库
* 复杂数据库查询需求须通过handler处理,注册的handler中会将已建好连接的gorm.DB对象带入

GORM使用参考文档：[https://gorm.io/zh_CN/docs]()
