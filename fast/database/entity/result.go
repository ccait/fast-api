package entity

type Result struct {
	Data  any   `json:"data"`
	Page  Page  `json:"page"`
	Total int64 `json:"total"`
}
