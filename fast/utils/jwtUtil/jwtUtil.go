package jwtUtil

import (
	"errors"
	"gitee.com/ccait-dev/fast-api/fast/utils/loggerUtil"
	"github.com/golang-jwt/jwt"
)

func Encode(jwtSecretKey string, content string) string {
	var token = jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["info"] = content
	tokenString, err := token.SignedString([]byte(jwtSecretKey))
	if err != nil {
		loggerUtil.Error(err.Error())
		return ""
	}
	return tokenString
}

func Decode(jwtSecretKey string, token string) (string, error) {
	var mySigningKey = []byte(jwtSecretKey)
	if token == "" {
		loggerUtil.Error("Missing token")
		return "", errors.New("Missing token")
	}

	claims := jwt.MapClaims{}
	tkn, err := jwt.ParseWithClaims(token, claims, func(*jwt.Token) (interface{}, error) {
		return mySigningKey, nil
	})

	if err != nil || !tkn.Valid {
		loggerUtil.Error("Invalid token!!!")
		return "", errors.New("Invalid token!!!")
	}
	return tkn.Claims.(jwt.MapClaims)["info"].(string), nil
}
