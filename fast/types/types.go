package types

import (
	"gitee.com/ccait-dev/fast-api/fast/config"
	"gitee.com/ccait-dev/fast-api/fast/database/entity"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type HandleMap map[string]HandleFunc

// HandleFunc defines the handler used by gin middleware as return value.
type HandleFunc func(ctx *gin.Context, params map[string]interface{}, db *gorm.DB, opts *Options)

type Options struct {
	Handles        HandleMap
	ModelMapper    map[string]interface{}
	OnBeforeStart  func(eng *gin.Engine, config *config.AppConfig)
	OnUpload       func(ctx *gin.Context, filename string, bytes []byte, config *config.AppConfig) (bool, string)
	OnDownload     func(ctx *gin.Context, filename string, file interface{}, config *config.AppConfig) bool
	OnCreate       func(ctx *gin.Context, params *map[string]interface{}, config *config.AppConfig) bool
	OnUpdate       func(ctx *gin.Context, params *map[string]interface{}, config *config.AppConfig) bool
	OnRead         func(ctx *gin.Context, data *entity.Result, config *config.AppConfig) bool
	OnDelete       func(ctx *gin.Context, params *map[string]interface{}, config *config.AppConfig) bool
	DisableFastApi bool
}
