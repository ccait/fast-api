package entity

type Page struct {
	PageSize   int `json:"pageSize"`
	PageIndex  int `json:"pageIndex"`
	TotalPages int `json:"totalPages"`
}
