package express

type ConditionInfo struct {
	Algorithm string
	Value     interface{}
	Name      string
}

type ConditionInfos []ConditionInfo
