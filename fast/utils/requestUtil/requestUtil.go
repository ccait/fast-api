package requestUtil

import (
	"encoding/json"
	"fmt"
	"gitee.com/ccait-dev/fast-api/fast/database/entity"
	"gitee.com/ccait-dev/fast-api/fast/utils/jsonUtil"
	"gitee.com/ccait-dev/fast-api/fast/utils/loggerUtil"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
	"io/ioutil"
	"net/http"
)

func GetData(c *gin.Context, fieldCase string) map[string]interface{} {
	reslut := make(map[string]interface{})
	if c.GetHeader("Content-Type") == "application/x-www-form-urlencoded" ||
		c.GetHeader("Enctype") == "application/x-www-form-urlencoded" {
		c.DefaultPostForm("id", "")
		// 使用PostFormMap获取表单数据
		for key, value := range c.Request.PostForm {
			reslut[getKey(key, fieldCase)] = value
		}
	} else if c.GetHeader("Content-Type") == "multipart/form-data" ||
		c.GetHeader("Enctype") == "multipart/form-data" {
		// 必须在调用MultipartForm之前调用Request.ParseMultipartForm
		err := c.Request.ParseMultipartForm(32 << 20) // 设置最大内存为32MB
		if err != nil {
			loggerUtil.Error(err.Error())
			return reslut
		}

		form, err := c.MultipartForm()
		if err != nil {
			loggerUtil.Error(err.Error())
			return reslut
		}

		// 遍历文本字段
		for key, value := range form.Value {
			reslut[getKey(key, fieldCase)] = value
		}

		// 遍历文件字段
		for key, fileHeaders := range form.File {
			for _, fileHeader := range fileHeaders {
				filename := fileHeader.Filename
				file, _ := fileHeader.Open()
				filebytes, err := ioutil.ReadAll(file)
				if err != nil {
					loggerUtil.Error(err.Error())
					return reslut
				}
				reslut[fmt.Sprintf("filename_%s", key)] = filename
				reslut[getKey(key, fieldCase)] = filebytes
			}
		}
	} else {
		reslut, _ = jsonUtil.GetData(c.Request.Body)
		if fieldCase == "UCamel" {
			reslut = jsonUtil.CamelCase(reslut, true)
		} else if fieldCase == "LCamel" {
			reslut = jsonUtil.CamelCase(reslut, false)
		} else {
			reslut = jsonUtil.SnakeCase(reslut)
		}
	}

	if len(reslut) == 0 {
		id, exists := c.GetQuery("id")
		if exists {
			if reslut == nil {
				reslut = map[string]interface{}{}
			}
			reslut["id"] = id
		}
	}

	return reslut
}

func getKey(field string, fieldCase string) string {
	if fieldCase == "UCamel" {
		return jsonUtil.Ucfirst(jsonUtil.Case2Camel(field))
	} else if fieldCase == "LCamel" {
		return jsonUtil.Lcfirst(jsonUtil.Case2Camel(field))
	}
	return jsonUtil.Camel2Case(field)
}

func GetQueryInfo(params map[string]interface{}) entity.Query {
	// 反序列化JSON到结构体
	result := entity.Query{}
	params = jsonUtil.CamelCase(params, false)
	err := json.Unmarshal([]byte(jsonUtil.ToJson(params)), &result)
	if err != nil {
		fmt.Println(err)
	}
	return result
}

/*
* 获取token
 */
func GetToken(c *gin.Context, signingKey string) jwt.MapClaims {
	token := c.Request.Header.Get("Authorization")
	claims := jwt.MapClaims{}
	var mySigningKey = []byte(signingKey)
	tkn, err := jwt.ParseWithClaims(token, claims, func(*jwt.Token) (interface{}, error) {
		return mySigningKey, nil
	})
	if err != nil || !tkn.Valid {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Invalid token!!!"})
		return nil
	}

	return claims
}
