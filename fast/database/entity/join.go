package entity

type Join struct {
	Table    string `json:"table"`
	Alias    string `json:"alias"`
	JoinMode string `json:"joinMode"`
	OnList   []On   `json:"onList"`
}
