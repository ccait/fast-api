package entity

type ResponseData struct {
	Result  Result `json:"result"`
	Message string `json:"message"`
	ErrCode int    `json:"errCode"`
}
