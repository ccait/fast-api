package idUtil

import (
	"math"
	"time"
)

// Snowflake ID 结构
type Snowflake struct {
	epoch        int64 // 起始时间戳，如 2021-01-01
	nodeBits     uint8 // 节点 ID 位数，如 10 位
	nodeMax      int64 // 节点 ID 最大值
	node         int64 // 当前节点 ID
	sequenceBits uint8 // 序列号位数，如 12 位
	sequenceMask int64 // 序列号掩码
}

// 初始化雪花 ID 结构
func NewSnowflake(nodeID int64, epoch int64, nodeBits uint8, sequenceBits uint8) *Snowflake {
	nodeMax := int64(-1) ^ (int64(-1) << nodeBits)
	sequenceMask := int64(-1) ^ (int64(-1) << sequenceBits)
	return &Snowflake{
		epoch:        epoch,
		nodeBits:     nodeBits,
		nodeMax:      nodeMax,
		node:         nodeID & nodeMax,
		sequenceBits: sequenceBits,
		sequenceMask: sequenceMask,
	}
}

// 假设这里使用了一个简单的内存位置来存储上次时间戳和序列号
var lastTimestamp int64
var sequence int64

func (s *Snowflake) getLastTimestamp() int64 {
	return lastTimestamp
}

func (s *Snowflake) setLastTimestamp(timestamp int64) {
	lastTimestamp = timestamp
}

func (s *Snowflake) getSequence() int64 {
	return sequence
}

func (s *Snowflake) setSequence(seq int64) {
	sequence = seq
}

// 创建雪花 ID
func CreateId() int64 {
	s := Snowflake{}
	// 获取当前时间戳
	timestamp := time.Now().UnixNano() / 1e6 // 毫秒级
	timestamp -= s.epoch

	// 时间戳超出范围则等待下一个时间周期
	for timestamp > (math.MaxInt64 >> (s.nodeBits + s.sequenceBits)) {
		time.Sleep(time.Millisecond)
		timestamp = time.Now().UnixNano() / 1e6 // 毫秒级
		timestamp -= s.epoch
	}

	// 生成序列号
	var sequence int64
	if lastTimestamp := s.getLastTimestamp(); timestamp == lastTimestamp {
		sequence = (s.getSequence() + 1) & s.sequenceMask
		if sequence == 0 {
			time.Sleep(time.Millisecond)
			timestamp = time.Now().UnixNano() / 1e6 // 毫秒级
			timestamp -= s.epoch
		}
	} else {
		sequence = 0
	}
	s.setLastTimestamp(timestamp)
	s.setSequence(sequence)

	// 组合 ID 并返回
	return (timestamp << (s.nodeBits + s.sequenceBits)) | (s.node << s.sequenceBits) | sequence
}

func baseConverter(value int64, base int) string {
	var digits = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	if base > len(digits) {
		panic("Invalid base")
	}

	var result string
	for value > 0 {
		result = string(digits[value%int64(base)]) + result
		value = value / int64(base)
	}
	return result
}

func ShortId(id int64) string {
	return baseConverter(id, 62)
}

func CreateShortId() string {
	return baseConverter(CreateId(), 62)
}
