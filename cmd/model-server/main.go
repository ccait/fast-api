package main

import (
	"gitee.com/ccait-dev/fast-api/cmd/model-server/model"
	"gitee.com/ccait-dev/fast-api/fast"
	. "gitee.com/ccait-dev/fast-api/fast/types"
)

func main() {
	server := fast.Server{}
	server.Start(&Options{
		ModelMapper: model.GetModelMapper(),
	})
}
