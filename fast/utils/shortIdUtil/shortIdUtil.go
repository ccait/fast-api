package shortIdUtil

import (
	"gitee.com/ccait-dev/fast-api/fast/utils/idUtil"
)

func baseConverter(value int64, base int) string {
	var digits = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	if base > len(digits) {
		panic("Invalid base")
	}

	var result string
	for value > 0 {
		result = string(digits[value%int64(base)]) + result
		value = value / int64(base)
	}
	return result
}

func ShortId(id int64) string {
	return baseConverter(id, 62)
}

func CreateShortId() string {
	return baseConverter(idUtil.CreateId(), 62)
}
