package types

type ColumnInfo struct {
	Field       string   `json:"field"`
	Name        string   `json:"name"`
	IsEdit      bool     `json:"isEdit" json:"is_edit"`
	Md5Encrypt  bool     `json:"md5Encrypt" json:"md5_encrypt"`
	Type        string   `json:"type"`
	Require     bool     `json:"require"`
	Disabled    bool     `json:"disabled"`
	IgnoreField bool     `json:"ignoreField" json:"ignore_field"`
	DictQuery   DictInfo `json:"dictQuery" json:"dict_query"`
}

type ColumnInfos []ColumnInfo

func (columns ColumnInfos) HasField(field string) bool {
	for _, column := range columns {
		if column.Field == field {
			return true
		}
	}
	return false
}
