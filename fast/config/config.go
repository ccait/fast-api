package config

import (
	"gopkg.in/yaml.v3"
	_ "gopkg.in/yaml.v3"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"gorm.io/plugin/prometheus"
	"os"
	"strings"
	"sync"
	"time"
)

var (
	Config *AppConfig
	lock   sync.RWMutex
)

func init() {
	// 初始化配置
	if Config == nil {
		var conf = AppConfig{}
		_, err := conf.Load()
		if err != nil {
			panic(err)
		}
	}
}

// 定义你的YAML结构
type RootConfig struct {
	Api AppConfig `yaml:"api"`
}

type AppConfig struct {
	Port               string           `yaml:"port"`
	Fast               any              `yaml:"fast"`
	Methods            string           `yaml:"methods"`
	FastGroup          string           `yaml:"fastGroup" yaml:"fast_group"`
	ServerName         string           `yaml:"serverName" yaml:"server_name"`
	Routes             []RouteConfig    `yaml:"routes"`
	Datasource         DatasourceConfig `yaml:"datasource"`
	DisableSnowflakeId bool             `yaml:"disableSnowflakeId" yaml:"disable_snowflake_id"`
	DisableMapper      bool             `yaml:"disableMapper" yaml:"disable_mapper"`
	FieldCase          string           `yaml:"fieldCase" yaml:"field_case"`
	UploadRootPath     string           `yaml:"uploadRootPath" yaml:"upload_root_path"`
	LoginTimeout       time.Duration    `yaml:"loginTimeout" yaml:"login_timeout"`
	UserSigningKey     string           `yaml:"userSigningKey" yaml:"user_signing_key"`
	AdminSigningKey    string           `yaml:"adminSigningKey" yaml:"admin_signing_key"`
}

type DatasourceConfig struct {
	Config      GOrmConfig          `yaml:"Config"`
	Pools       PoolsConfig         `yaml:"pools"`
	Connections map[string]DBConfig `yaml:"connections"`
	Prometheus  prometheus.Config   `yaml:"prometheus"`
}

// Config GORM Config
type GOrmConfig struct {
	// GORM perform single create, update, delete operations in transactions by default to ensure database data integrity
	// You can disable it by setting `SkipDefaultTransaction` to true
	SkipDefaultTransaction bool `yaml:"skipDefaultTransaction"`
	// NamingStrategy tables, columns naming strategy
	NamingStrategy schema.Namer `yaml:"namingStrategy"`
	// FullSaveAssociations full save associations
	FullSaveAssociations bool `yaml:"fullSaveAssociations,omitempty"`
	// Logger
	Logger logger.Interface `yaml:"logger,omitempty"`
	// NowFunc the function to be used when creating a new timestamp
	NowFunc func() time.Time `yaml:"nowFunc"`
	// DryRun generate sql without execute
	DryRun bool `yaml:"dryRun"`
	// PrepareStmt executes the given query in cached statement
	PrepareStmt bool `yaml:"prepareStmt"`
	// DisableAutomaticPing
	DisableAutomaticPing bool `yaml:"disableAutomaticPing"`
	// DisableForeignKeyConstraintWhenMigrating
	DisableForeignKeyConstraintWhenMigrating bool `yaml:"disableForeignKeyConstraintWhenMigrating"`
	// IgnoreRelationshipsWhenMigrating
	IgnoreRelationshipsWhenMigrating bool `yaml:"ignoreRelationshipsWhenMigrating"`
	// DisableNestedTransaction disable nested transaction
	DisableNestedTransaction bool `yaml:"disableNestedTransaction"`
	// AllowGlobalUpdate allow global update
	AllowGlobalUpdate bool `yaml:"allowGlobalUpdate"`
	// QueryFields executes the SQL query with all fields of the table
	QueryFields bool `yaml:"queryFields"`
	// CreateBatchSize default create batch size
	CreateBatchSize int `yaml:"createBatchSize"`
	// TranslateError enabling error translation
	TranslateError bool `yaml:"translateError"`
	// PropagateUnscoped propagate Unscoped to every other nested statement
	PropagateUnscoped bool `yaml:"propagateUnscoped"`

	// ClauseBuilders clause builder
	ClauseBuilders map[string]clause.ClauseBuilder `yaml:"clauseBuilders"`
	CacheStore     *sync.Map                       `yaml:"cacheStore"`
}

func (G GOrmConfig) Apply(c *gorm.Config) *gorm.Config {
	c.AllowGlobalUpdate = G.AllowGlobalUpdate
	c.DisableNestedTransaction = G.DisableNestedTransaction
	c.ClauseBuilders = G.ClauseBuilders
	c.IgnoreRelationshipsWhenMigrating = G.IgnoreRelationshipsWhenMigrating
	c.CreateBatchSize = G.CreateBatchSize
	c.TranslateError = G.TranslateError
	c.PropagateUnscoped = G.PropagateUnscoped
	c.DisableAutomaticPing = G.DisableAutomaticPing
	c.QueryFields = G.QueryFields
	c.DryRun = G.DryRun
	c.PrepareStmt = G.PrepareStmt
	c.FullSaveAssociations = G.FullSaveAssociations
	c.Logger = G.Logger
	c.NowFunc = G.NowFunc
	c.SkipDefaultTransaction = G.SkipDefaultTransaction
	c.NamingStrategy = G.NamingStrategy
	c.ClauseBuilders = G.ClauseBuilders
	c.DisableForeignKeyConstraintWhenMigrating = G.DisableForeignKeyConstraintWhenMigrating
	return c
}

func (G GOrmConfig) AfterInitialize(db *gorm.DB) error {
	//TODO implement me
	panic("implement me")
}

type PoolsConfig struct {
	MaxOpenConns int `yaml:"maxOpenConns"`
	MaxIdleConns int `yaml:"maxIdleConns"`
	ConnLifeTime int `yaml:"connLifeTime"`
	ConnIdleTime int `yaml:"connIdleTime"`
}

type DBConfig struct {
	ConnectString string `yaml:"connectString"`
	Driver        string `yaml:"driver"`
}

type RouteConfig struct {
	Url     string `yaml:url`
	Method  string `yaml:"method"`
	Handler string `yaml:"handler"`
}

func (conf *AppConfig) Load() (*AppConfig, error) {
	if Config != nil {
		return conf, nil
	}
	lock.RLock()
	defer lock.RUnlock()
	file, err := os.Open("./config/application.yml")
	if err != nil {
		return &AppConfig{}, err
	}
	defer file.Close()
	decoder := yaml.NewDecoder(file)
	var rootConfig RootConfig
	err = decoder.Decode(&rootConfig)
	if err != nil {
		return nil, err
	}
	conf.Fast = rootConfig.Api.Fast
	conf.Methods = rootConfig.Api.Methods
	if strings.HasPrefix(rootConfig.Api.FastGroup, "/") {
		conf.FastGroup = "/" + rootConfig.Api.FastGroup
	} else {
		conf.FastGroup = rootConfig.Api.FastGroup
	}

	conf.DisableSnowflakeId = rootConfig.Api.DisableSnowflakeId
	conf.DisableMapper = rootConfig.Api.DisableMapper
	conf.Port = rootConfig.Api.Port
	conf.Routes = rootConfig.Api.Routes
	conf.UploadRootPath = rootConfig.Api.UploadRootPath
	conf.LoginTimeout = rootConfig.Api.LoginTimeout
	conf.UserSigningKey = rootConfig.Api.UserSigningKey
	conf.AdminSigningKey = rootConfig.Api.AdminSigningKey
	conf.Datasource = rootConfig.Api.Datasource
	conf.ServerName = rootConfig.Api.ServerName
	if strings.TrimSpace(conf.AdminSigningKey) == "" {
		conf.AdminSigningKey = "CCAITDEVFastApiForAdmin"
	}
	if strings.TrimSpace(conf.UserSigningKey) == "" {
		conf.UserSigningKey = "CCAITDEVFastApiForUser"
	}
	Config = conf
	return conf, nil
}
