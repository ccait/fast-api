package express

import (
	"fmt"
	"gitee.com/ccait-dev/fast-api/fast/utils/jsonUtil"
	"regexp"
	"strings"
)

/***
 * 格式：${1,2,3,4,5...}
 */
const MATCH_EL_IN = "^\\s*\\$\\{(\\d+(,\\s*\\d+)*)\\}\\s*$"

/***
 * 格式：${1-5, 7-9, ...}
 */
const MATCH_EL_REGION = "^\\s*\\$\\{((\\d+\\-\\d+)(,\\s*\\d+\\-\\d+\\s*)*)\\}\\s*$"

/***
 * 格式：${yyyy-MM-dd HH:mm:ss ~ yyyy-MM-dd HH:mm:ss, ...}
 */
const MATCH_DATETIME_REGION = "^\\s*\\$\\{\\s*(((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})\\-(((0[13578]|1[02])\\-(0[1-9]|[12][0-9]|3[01]))|" +
	"((0[469]|11)\\-(0[1-9]|[12][0-9]|30))|(02\\-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|" +
	"((0[48]|[2468][048]|[3579][26])00))\\-02\\-29))\\s([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])\\s*\\~\\s*((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})\\-(((0[13578]|1[02])\\-(0[1-9]|[12][0-9]|3[01]))|" +
	"((0[469]|11)\\-(0[1-9]|[12][0-9]|30))|(02\\-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|" +
	"((0[48]|[2468][048]|[3579][26])00))\\-02\\-29))\\s([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]))\\s*\\}$"

/***
 * 格式：${yyyy-MM-dd HH:mm:ss !~ yyyy-MM-dd HH:mm:ss, ...}
 */
const MATCH_DATETIME_REGION_NOT = "^\\s*\\$\\{\\s*(((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})\\-(((0[13578]|1[02])\\-(0[1-9]|[12][0-9]|3[01]))|" +
	"((0[469]|11)\\-(0[1-9]|[12][0-9]|30))|(02\\-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|" +
	"((0[48]|[2468][048]|[3579][26])00))\\-02\\-29))\\s([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])\\s*\\!\\~\\s*((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})\\-(((0[13578]|1[02])\\-(0[1-9]|[12][0-9]|3[01]))|" +
	"((0[469]|11)\\-(0[1-9]|[12][0-9]|30))|(02\\-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|" +
	"((0[48]|[2468][048]|[3579][26])00))\\-02\\-29))\\s([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]))\\s*\\}$"

/***
 * 格式：${yyyy/MM/dd HH:mm:ss - yyyy/MM/dd HH:mm:ss, ...}
 */
const MATCH_DATETIME2_REGION = "^\\s*\\$\\{\\s*(((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})\\/(((0[13578]|1[02])\\/(0[1-9]|[12][0-9]|3[01]))|" +
	"((0[469]|11)\\/(0[1-9]|[12][0-9]|30))|(02\\/(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|" +
	"((0[48]|[2468][048]|[3579][26])00))\\/02\\/29))\\s([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])\\s*\\-\\s*((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})\\/(((0[13578]|1[02])\\/(0[1-9]|[12][0-9]|3[01]))|" +
	"((0[469]|11)\\/(0[1-9]|[12][0-9]|30))|(02\\/(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|" +
	"((0[48]|[2468][048]|[3579][26])00))\\/02\\/29))\\s([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]))\\s*\\}$"

const MATCH_ADVANCE_SEARCH = "\\$\\{\\s*((([\\w\\$][\\w\\d_]*)\\s*( (CONTAINS|BEGIN|END) |=|>|<|>=|<=)\\s*([^|&\\s\\{\\}]+))\\s*([\\|\\&]\\s*(\\w[\\w\\d_]*)\\s*( (CONTAINS|BEGIN|END) |=|>|<|>=|<=)\\s*([^|&\\s\\{\\}]+))*)\\s*\\}"

const MATCH_ADVANCE_SEARCH_SUB_GROUP = "(([\\w\\$][\\w\\d_]*)\\s*( (CONTAINS|BEGIN|END) |=|>|<|>=|<=)\\s*([^,\\{\\}]+))"

var (
	algorithmMap = map[string]interface{}{
		"=":        "",
		">":        "",
		"<":        "",
		">=":       "",
		"<=":       "",
		"!=":       "",
		"<>":       "",
		"BEGIN":    "",
		"END":      "",
		"CONTAINS": "",
	}
)

func GetAlgorithm(alg string) string {
	if !jsonUtil.ContainsKey(algorithmMap, alg) {
		return ""
	}
	return alg
}

func ParseElExpr(field string, val string) string {
	if strings.HasPrefix(field, "$") {
		field = field[1:len(field)]
	} else {
		return ReplaceInjectChar(val)
	}
	regex, err := regexp.Compile(MATCH_EL_IN)
	if err != nil {
		return ReplaceInjectChar(val)
	}
	m := regex.FindStringSubmatch(val)
	if m != nil && len(m) > 1 {
		s, done := GetIn(field, val, m)
		if done {
			return s
		}
	}

	regex, err = regexp.Compile(MATCH_EL_REGION)
	if err != nil {
		return ReplaceInjectChar(val)
	}
	m = regex.FindStringSubmatch(val)
	if m != nil && len(m) > 1 {
		s, done := GetBetween(field, val, m, "-")
		if done {
			return s
		}
	}

	regex, err = regexp.Compile(MATCH_DATETIME_REGION)
	if err != nil {
		return ReplaceInjectChar(val)
	}
	m = regex.FindStringSubmatch(val)
	if m != nil && len(m) > 1 {
		s, done := GetBetween(field, val, m, "~")
		if done {
			return s
		}
	}

	regex, err = regexp.Compile(MATCH_DATETIME_REGION_NOT)
	if err != nil {
		return ReplaceInjectChar(val)
	}
	m = regex.FindStringSubmatch(val)
	if m != nil && len(m) > 1 {
		s, done := GetBetween(field, val, m, "!~")
		if done {
			return s
		}
	}

	regex, err = regexp.Compile(MATCH_DATETIME2_REGION)
	if err != nil {
		return ReplaceInjectChar(val)
	}
	m = regex.FindStringSubmatch(val)
	if m != nil && len(m) > 1 {
		s, done := GetBetween(field, val, m, "-")
		if done {
			return s
		}
	}

	regex, err = regexp.Compile(MATCH_ADVANCE_SEARCH)
	if err != nil {
		return ReplaceInjectChar(val)
	}
	m = regex.FindStringSubmatch(val)
	if m != nil && len(m) > 1 && len(strings.TrimSpace(m[1])) > 0 {
		regex, err = regexp.Compile(MATCH_ADVANCE_SEARCH_SUB_GROUP)
		if err != nil {
			return ReplaceInjectChar(val)
		}
		m2 := regex.FindAllStringSubmatch(val, 99)
		if m2 != nil {
			var conds []string
			for _, mm := range m2 {
				var cond = ConditionInfo{}
				cond.Name = ReplaceInjectChar(mm[2])
				cond.Name = strings.ReplaceAll(strings.TrimSpace(cond.Name), "$$", field)
				if strings.HasPrefix(cond.Name, "$") {
					cond.Name = FieldCaseTo(cond.Name[1:len(cond.Name)])
				} else {
					cond.Name = fmt.Sprintf("'%s'", cond.Name)
				}
				alg := mm[4]
				if !jsonUtil.ContainsKey(algorithmMap, alg) {
					fmt.Printf("can not support algorithm %s\n", val)
					return ReplaceInjectChar(val)
				}

				cond.Algorithm = alg
				if len(strings.TrimSpace(mm[5])) > 0 {
					val = ReplaceInjectChar(strings.TrimSpace(mm[5]))
					val = strings.ReplaceAll(strings.TrimSpace(val), "$$", field)
					if strings.HasPrefix(val, "$") {
						val = ReplaceInjectChar(FieldCaseTo(val[1:len(val)]))
					} else {
						val = fmt.Sprintf("'%s'", ReplaceInjectChar(val))
					}
				}
				cond.Value = ReplaceInjectChar(val)
				if alg == "CONTAINS" {
					conds = append(conds, fmt.Sprintf("%s LIKE '%s'", cond.Name, "%"+TrimInjectChar(val)+"%"))
				} else if alg == "BEGIN" {
					conds = append(conds, fmt.Sprintf("%s LIKE '%s'", cond.Name, TrimInjectChar(val)+"%"))
				} else if alg == "END" {
					conds = append(conds, fmt.Sprintf("%s LIKE '%s'", cond.Name, "%"+TrimInjectChar(val)))
				} else {
					conds = append(conds, fmt.Sprintf("%s%s%s", cond.Name, cond.Algorithm, cond.Value))
				}
			}
			if len(conds) > 0 {
				return strings.Join(conds, " OR ")
			} else {
				return ReplaceInjectChar(val)
			}
		}
	}
	return ReplaceInjectChar(val)
}
