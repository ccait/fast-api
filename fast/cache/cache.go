package cache

import (
	"sync"
	"time"
)

var (
	cache *Cache
)

func init() {
	cache = NewCache()
}

type Cache struct {
	sync.Map
}

// 缓存结构体
type CacheItem struct {
	value  interface{}
	expire *time.Time
}

func NewCache() *Cache {
	return &Cache{}
}

func Get(key string) (interface{}, bool) {
	return cache.Get(key)
}

func Set(key string, value interface{}, duration time.Duration) {
	cache.Set(key, value, duration)
}

func (c *Cache) Get(key string) (interface{}, bool) {
	obj, ok := c.Map.Load(key)
	if !ok || obj == nil {
		return nil, false
	}
	item, ok := obj.(CacheItem)
	if !ok {
		return nil, false
	}
	if item.expire != nil && item.expire.Before(time.Now()) {
		c.Map.Delete(key)
		return nil, false
	}
	return item.value, true
}

func (c *Cache) Set(key string, value interface{}, duration time.Duration) {
	t := time.Now().Add(duration)
	item := CacheItem{value: value, expire: &t}
	if duration == 0 {
		item.expire = nil
	}
	c.Map.Store(key, item)
}
