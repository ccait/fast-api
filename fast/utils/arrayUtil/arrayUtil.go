package arrayUtil

import (
	"reflect"
)

func Contains(arr interface{}, e interface{}) bool {
	for _, a := range ToArray(arr) {
		if a == e {
			return true
		}
	}
	return false
}

func ToArray(data interface{}) []interface{} {
	// 使用反射获取数组的元素类型
	arrValue := reflect.ValueOf(data)
	if arrValue.Kind() != reflect.Array && arrValue.Kind() != reflect.Slice {
		return nil
	}

	// 创建一个与原数组类型相同的空切片
	arr := reflect.MakeSlice(reflect.TypeOf([]interface{}{}), 0, arrValue.Len())
	for i := 0; i < arrValue.Len(); i++ {
		arr = reflect.Append(arr, arrValue.Index(i))
	}

	// 将 interface{} 转换为 []interface{} 类型
	interfaceArray := arr.Interface().([]interface{})

	return interfaceArray
}
