package loggerUtil

import (
	"errors"
	"fmt"
	"github.com/logrusorgru/aurora"
	"log"
	"os"
	"strings"
)

var (
	logger   *log.Logger
	logFile  *os.File
	filename = ""
)

func init() {
	logFile, err := OpenFile(filename)
	defer logFile.Close()

	// 创建日志
	logger = log.New(logFile, "INFO: ", log.LstdFlags)

	err = logFile.Truncate(0)
	if err != nil {
		fmt.Println(err.Error())
	}
	// 写入日志
	logger.Println("logger init!")
}

func OpenFile(filepath string) (*os.File, error) {
	if logFile != nil && logFile.Name() != "" {
		return logFile, nil
	}
	if strings.TrimSpace(filepath) != "" {
		filename = strings.TrimSpace(filepath)
	}
	if strings.TrimSpace(filename) == "" {
		filename = "app.log"
	}
	// 打开日志文件
	logFile, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
	}
	return logFile, err
}

func Error(format string, v ...any) error {
	Log(aurora.BgRed, format, v)
	if v != nil && len(v) > 0 {
		return Throws(v)
	}
	return nil
}

func Success(format string, v ...any) {
	Log(aurora.Cyan, format, v)
}

func Waring(format string, v ...any) {
	Log(aurora.Yellow, format, v)
}

func Log(callback func(arg interface{}) aurora.Value, format string, v ...any) {
	logFile, err := OpenFile(filename)
	if err != nil {
		fmt.Println(getMessage(aurora.BgRed, "Logger error: ", err.Error()))
	} else {
		logger.SetOutput(logFile)
		defer logFile.Close()
	}
	if format != "" && len(v) > 0 {
		fmt.Println(getMessagef(callback, format, v))
		logger.Println(getMessagef(callback, format, v))
	} else {
		if len(v) > 0 {
			fmt.Println(getMessage(callback, v))
			logger.Println(getMessage(callback, v))
		} else {
			fmt.Println(getMessage(callback, format))
			logger.Println(getMessage(callback, format))
		}
	}
}

func getMessage(callback func(arg interface{}) aurora.Value, v ...any) any {
	var msg []string
	if len(v) > 0 {
		for _, a := range v {
			if a == nil || fmt.Sprintf("%v", a) == "[[]]" {
				continue
			}
			msg = append(msg, fmt.Sprintf("%v", a))
		}
	}
	if callback != nil {
		return callback("::> " + strings.Join(msg, " \n") + " ")
	}
	return strings.Join(msg, " \n")
}

func getMessagef(callback func(arg interface{}) aurora.Value, format string, v ...any) any {
	if strings.TrimSpace(format) != "" {
		msg := ""
		if len(v) > 0 {
			for _, a := range v {
				if a == nil || fmt.Sprintf("%v", a) == "[[]]" {
					continue
				}
				msg = msg + "\n" + fmt.Sprintf("%v", a)
			}
		}
		if strings.TrimSpace(msg) == "" {
			msg = format
		} else {
			msg = format + "\n" + msg
		}
		if callback != nil {
			return callback("::> " + msg + " ")
		}
		return msg
	} else {
		return getMessage(callback, v...)
	}
}

func Throws(v []any) error {
	if len(v) > 0 {
		var msgs []string
		for _, a := range v {
			msgs = append(msgs, fmt.Sprintf("%v", a))
		}
		return errors.New(strings.Join(msgs, "\n"))
	}
	return nil
}
