package excelUtil

import (
	"errors"
	"gitee.com/ccait-dev/fast-api/fast/types"
	"gitee.com/ccait-dev/fast-api/fast/utils/jsonUtil"
	"github.com/gin-gonic/gin"
	"github.com/tealeg/xlsx"
	"net/http"
	"reflect"
	"strings"
)

func ExportSheet(c *gin.Context, sheetName string, data interface{}, columns []types.ColumnInfo) {
	file, sheetName, err := Create(sheetName, data, columns)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	Export(sheetName+`.xlsx`, c, file)
}

func Create(sheetName string, data interface{}, columns []types.ColumnInfo) (*xlsx.File, string, error) {
	file := xlsx.NewFile()
	if strings.TrimSpace(sheetName) == "" {
		sheetName = "Sheet1"
	}
	_, err := CreateSheet(file, sheetName, data, columns)
	return file, sheetName, err
}

func Export(filename string, c *gin.Context, file *xlsx.File) {
	// 设置HTTP响应头
	c.Header("Content-Type", "application/vnd.ms-excel")
	c.Header("Content-Disposition", `attachment; filename=`+filename)
	c.Status(http.StatusOK)
	// 将文件内容发送到客户端
	err := file.Write(c.Writer)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}
}

func GetColumnFromSheet(sheet *xlsx.Sheet) ([]types.ColumnInfo, error) {
	columns := make([]types.ColumnInfo, 0)
	if len(sheet.Rows) < 2 {
		return columns, errors.New("Length can not be zroe.")
	}
	for i, _ := range sheet.Rows[1].Cells {
		if strings.TrimSpace(sheet.Rows[1].Cells[i].Value) == "" {
			continue
		}
		columns = append(columns, types.ColumnInfo{
			Field: sheet.Rows[1].Cells[i].Value,
			Name:  sheet.Rows[0].Cells[i].Value,
		})
	}
	return columns, nil
}

func GetDataFromSheet(sheet *xlsx.Sheet, startRowNum int, columns []types.ColumnInfo) ([]map[string]interface{}, error) {
	data := make([]map[string]interface{}, 0)
	if len(sheet.Rows) == 0 || len(columns) == 0 {
		return data, errors.New("Length can not be zroe.")
	}
	if startRowNum < 0 || startRowNum > len(sheet.Rows)-1 {
		return data, errors.New("Start row number over bound.")
	}
	for i, _ := range sheet.Rows {
		if i < startRowNum {
			continue
		}
		row := make(map[string]interface{})
		for j, column := range columns {
			value := sheet.Cell(i, j).Value
			field := column.Field
			if strings.TrimSpace(field) != "" {
				row[field] = value
				data = append(data, row)
			}
		}
	}
	return data, nil
}

func CreateSheet(file *xlsx.File, sheetName string, data interface{}, columns []types.ColumnInfo) (*xlsx.Sheet, error) {
	// 添加一个工作表
	sheet, err := file.AddSheet(sheetName)
	if err != nil {
		return nil, err
	}

	for i, column := range columns {
		// 写入表头到工作表
		sheet.Cell(0, i).SetString(column.Name)
	}

	arr := reflect.ValueOf(data)
	if arr.Kind() != reflect.Slice {
		return nil, errors.New("传入的不是数组")
	}

	for j := 0; j < arr.Len(); j++ {
		elem := arr.Index(j)
		//fmt.Printf("索引 %d: 类型 = %arr, 值 = %arr\n", j, elem.Type(), elem.Interface())
		for i, column := range columns {
			if strings.TrimSpace(column.Field) == "" {
				continue
			}
			field := elem.FieldByName(jsonUtil.Case2Camel(column.Field))
			if field.Kind() == reflect.Ptr && field.IsNil() {
				continue
			}
			if !field.IsValid() {
				continue
			}
			// 写入工作表
			sheet.Cell(j+1, i).SetValue(elem.FieldByName(jsonUtil.Case2Camel(column.Field)))
		}
	}

	return sheet, nil
}
