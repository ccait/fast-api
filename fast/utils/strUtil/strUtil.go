package strUtil

import (
	"fmt"
	"regexp"
	"strings"
	"time"
)

func IsDatetime(dateTime string) bool {
	if strings.TrimSpace(dateTime) == "" || (strings.Contains(dateTime, "/") &&
		strings.Contains(dateTime, "-")) {
		return false
	}
	// 正则表达式，匹配YYYY-MM-DD HH:MM:SS格式
	regex := regexp.MustCompile(`^\d{4}[-/]\d{1,2}([-/]\d{1,2})?([ T](\d{2}:\d{2}(:\d{2})?(\.\d{3})?Z?)?)?$`)
	if regex.MatchString(dateTime) {
		return true
	}
	regex = regexp.MustCompile(`^(\d{1,2}[-/])?\d{1,2}[-/]\d{4}([ T](\d{2}:\d{2}(:\d{2})?(\.\d{3})?Z?)?)?$`)
	if regex.MatchString(dateTime) {
		return true
	}
	return false
}

func ParseTimestamp(dateStr string, layout string) int64 {
	if strings.TrimSpace(dateStr) != "" && IsDatetime(dateStr) {
		if strings.TrimSpace(layout) == "" {
			layout = "2006-01-02 15:04:05"
		} else {
			layout = strings.Replace(layout, "yyyy", "2006", -1)
			layout = strings.Replace(layout, "MM", "01", -1)
			layout = strings.Replace(layout, "dd", "02", -1)
			layout = strings.Replace(layout, "HH", "15", -1)
			layout = strings.Replace(layout, "mm", "04", -1)
			layout = strings.Replace(layout, "ss", "05", -1)
			layout = strings.Replace(layout, "SSS", "000", -1)
		}

		t, err := time.Parse(layout, dateStr)
		if err != nil {
			return 0
		}
		return t.Unix()
	}
	return 0
}

func ToString(val any) string {
	str, ok := val.(string)
	if ok {
		return str
	}
	b, ok := val.(bool)
	if ok {
		if b {
			return "true"
		} else {
			return "false"
		}
	}
	return fmt.Sprintf("%v", val)
}
