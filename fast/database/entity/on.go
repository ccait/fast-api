package entity

type On struct {
	Name      string `json:"name"`
	Value     string `json:"value"`
	Algorithm string `json:"algorithm"`
}
