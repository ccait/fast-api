package excelUtil

type ColumnInfo struct {
	Field   string `json:"field"`
	Name    string `json:"name"`
	IsEdit  bool   `json:"is_edit" json:"isEdit"`
	Type    string `json:"type"`
	Require bool   `json:"require"`
}
