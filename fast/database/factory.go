package database

import (
	"errors"
	"gitee.com/ccait-dev/fast-api/fast/config"
	"gitee.com/ccait-dev/fast-api/fast/utils/loggerUtil"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
	"strings"
	"sync"
	"time"
)

var (
	dbMap map[string]*gorm.DB
	lock  sync.RWMutex
)

type DBFactory struct{}

func (ds DBFactory) GetDB(dbname string) (*gorm.DB, error) {
	if len(strings.TrimSpace(dbname)) == 0 {
		if len(config.Config.Datasource.Connections) == 0 {
			return nil, errors.New("dbname is empty")
		} else {
			for key := range config.Config.Datasource.Connections {
				dbname = key
				break
			}
		}
	}
	if dbMap == nil {
		dbMap = make(map[string]*gorm.DB)
	}

	value, ok := dbMap[dbname]
	if ok {
		// dbname 存在
		return value, nil
	}

	lock.RLock()
	defer lock.RUnlock()

	var conf = config.Config
	dbConf, ok2 := conf.Datasource.Connections[dbname]
	if !ok2 {
		return nil, errors.New("connections not exist")
	}
	if len(strings.TrimSpace(dbConf.Driver)) == 0 {
		return nil, errors.New("dbname is empty")
	}
	if len(strings.TrimSpace(dbConf.ConnectString)) == 0 {
		return nil, errors.New("connectString is empty")
	}

	// 连接数据库
	switch dbConf.Driver {
	case "postgres":
		dbMap[dbname] = connect(postgres.Open(dbConf.ConnectString), &conf.Datasource)
		break
	case "mysql":
		dbMap[dbname] = connect(mysql.Open(dbConf.ConnectString), &conf.Datasource)
		break
	case "sqlite":
		dbMap[dbname] = connect(sqlite.Open(dbConf.ConnectString), &conf.Datasource)
		break
	case "sqlserver":
		dbMap[dbname] = connect(sqlserver.Open(dbConf.ConnectString), &conf.Datasource)
		break
	default:
		break
	}

	return dbMap[dbname], nil
}

func connect(dialector gorm.Dialector, conf *config.DatasourceConfig) *gorm.DB {
	db, err := gorm.Open(dialector, conf.Config.Apply(&gorm.Config{}))
	if err != nil {
		loggerUtil.Error(err.Error())
		return nil
	}
	// 自动迁移数据库结构
	// db.AutoMigrate(&YourModel{})

	setPools(db, conf)
	//sqlDB, _ := db.DB()
	//defer sqlDB.Close()
	return db
}

func setPools(db *gorm.DB, dsConf *config.DatasourceConfig) {
	// 将 GORM 的 DB 转换为 sql.DB
	sqlDB, err := db.Debug().DB()
	if err != nil {
		loggerUtil.Error(err.Error())
		return
	}
	// 设置连接池的最大空闲连接数和最大打开连接数
	sqlDB.SetMaxIdleConns(dsConf.Pools.MaxIdleConns)
	sqlDB.SetMaxOpenConns(dsConf.Pools.MaxOpenConns)
	sqlDB.SetConnMaxLifetime(time.Duration(dsConf.Pools.ConnLifeTime))
	sqlDB.SetConnMaxIdleTime(time.Duration(dsConf.Pools.ConnIdleTime))
}
