package entity

type Query struct {
	Count   bool                     `json:"count"`
	Export  bool                     `json:"export"`
	Page    Page                     `json:"page"`
	Query   []map[string]interface{} `json:"query"`
	Joins   []Join                   `json:"joins"`
	OrderBy map[string]string        `json:"orderBy"`
	Select  []string                 `json:"select"`
}
